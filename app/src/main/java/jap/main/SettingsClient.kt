package jap.main

import android.content.Context
import android.content.SharedPreferences

class SettingsClient(ctx: Context) {
    private var appCfg: SharedPreferences = ctx.getSharedPreferences(
        "APP_PREFERENCES", Context.MODE_PRIVATE
    )

    fun saveMemoryCardsNumber(num: Int) {
        appCfg.edit().putInt("MEMORY_CARDS_NUMBER", num).apply()
    }

    fun getMemoryCardsNumber(): Int {
        return appCfg.getInt("MEMORY_CARDS_NUMBER", 0)
    }

    fun setMemoryCardsTag(tag: String) {
        appCfg.edit().putString("MEMORY_CARDS_TAG", tag).apply()
    }

    fun getMemoryCardsTag(): String {
        return appCfg.getString("MEMORY_CARDS_TAG", "Все") ?: "Все"
    }

    fun setEveryLoginUpgradeCommonDictionaryState(state: Boolean) {
        appCfg.edit().putBoolean("EVERY_LOGIN_UPGRADE_COMMON_DICTIONARY_STATE", state).apply()
    }

    fun getEveryLoginUpgradeCommonDictionaryState(): Boolean {
        return appCfg.getBoolean("EVERY_LOGIN_UPGRADE_COMMON_DICTIONARY_STATE", true)
    }

    fun setEveryLoginUpgradeAudioForUserState(state: Boolean) {
        appCfg.edit().putBoolean("EVERY_LOGIN_UPGRADE_AUDIO_FOR_USER_STATE", state).apply()
    }

    fun getEveryLoginUpgradeAudioForUserState(): Boolean {
        return appCfg.getBoolean("EVERY_LOGIN_UPGRADE_AUDIO_FOR_USER_STATE", true)
    }

    fun setAutoDownloadAudioForUserDictionaryAfterState(state: Boolean) {
        appCfg.edit().putBoolean("AUTO_DOWNLOAD_AUDIO_FOR_USER_DICTIONARY_AFTER_STATE", state)
            .apply()
    }

    fun getAutoDownloadAudioForUserDictionaryAfterState(): Boolean {
        return appCfg.getBoolean("AUTO_DOWNLOAD_AUDIO_FOR_USER_DICTIONARY_AFTER_STATE", true)
    }

    fun setAutoRemoveUnusedAudioByUserDictionaryAfterReturnState(state: Boolean) {
        appCfg.edit()
            .putBoolean("AUTO_REMOVE_UNUSED_AUDIO_BY_USER_DICTIONARY_AFTER_RETURN_STATE", state)
            .apply()
    }

    fun getAutoRemoveUnusedAudioByUserDictionaryAfterReturnState(): Boolean {
        return appCfg.getBoolean(
            "AUTO_REMOVE_UNUSED_AUDIO_BY_USER_DICTIONARY_AFTER_RETURN_STATE",
            true
        )
    }

    fun setAudioPlayingState(state: Boolean) {
        appCfg.edit().putBoolean("AUDIO_PLAYING_STATE", state).apply()
    }

    fun getAudioPlayingState(): Boolean {
        return appCfg.getBoolean("AUDIO_PLAYING_STATE", true)
    }
}
