package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class MenuDictionary : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_menu_dictionary, container, false)
        v.findViewById<Button>(R.id.btnCommonDictionary).setOnClickListener {
            actInterface?.callNextFragment("CommonDictionary")
        }
        v.findViewById<Button>(R.id.btnPersonalDictionary).setOnClickListener {
            actInterface?.callNextFragment("PersonalDictionary")
        }
        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("MainMenu")
        }
        return v
    }
}