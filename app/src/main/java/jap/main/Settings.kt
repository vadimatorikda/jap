package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class Settings : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_settings, container, false)

        v.findViewById<Button>(R.id.btnOption).setOnClickListener {
            actInterface?.callNextFragment("SettingsOptions")
        }
        v.findViewById<Button>(R.id.btnActions).setOnClickListener {
            actInterface?.callNextFragment("SettingsActions")
        }
        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("MainMenu")
        }
        return v
    }
}
