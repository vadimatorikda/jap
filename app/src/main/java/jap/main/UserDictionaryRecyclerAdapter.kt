package jap.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserDictionaryRecyclerAdapter(
    private val context: Context,
    tag: String,
    type: DatabaseClient.SortType
) :
    RecyclerView.Adapter<UserDictionaryRecyclerAdapter.MyViewHolder>() {
    private val userDb = (if (tag.isEmpty()) {
        DatabaseClient.getSortCommonDatabaseForUser(String(), type)
    } else {
        DatabaseClient.getSortCommonDatabaseForUser(tag, type)
    })

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv: TextView = itemView.findViewById(R.id.tv)
        val btnPlayAgain: Button = itemView.findViewById(R.id.btnPlayAgain)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_dictionary_recyclerview_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val i = userDb[position]
        var str = i.jp + '\n'
        if (i.kanji.isNotEmpty()) {
            str += i.kanji + '\n'
        }

        for (ruItem in i.ru) {
            str += "$ruItem\n"
        }

        val item = DatabaseClient.getUserDatabaseItem(i.id)
        if (i.kanji.isNotEmpty()) {
            str += "JTR:\t" + item.jpToRuCounter.toString() + "%" + '\t'
            str += "KTJ:\t" + item.kanjiToJpCounter.toString() + "%" + '\t'
            str += "KTR:\t" + item.kanjiToRuCounter.toString() + "%" + '\n'
            str += "RTJ:\t" + item.ruToJpCounter.toString() + "%" + '\t'
            str += "JTK:\t" + item.jpToKanjiCounter.toString() + "%" + '\t'
            str += "RTK:\t" + item.ruToKanjiCounter.toString() + "%"
        } else {
            str += "JTR:\t" + item.jpToRuCounter.toString() + "%" + '\t'
            str += "RTJ:\t" + item.ruToJpCounter.toString() + "%"
        }

        holder.tv.text = str

        holder.btnPlayAgain.setOnClickListener(null)
        if (AudioClient.isAudioExist(i.jp)) {
            holder.btnPlayAgain.setOnClickListener {
                AudioClient.playAudio(context, i.jp)
            }
            holder.btnPlayAgain.visibility = View.VISIBLE
        } else {
            holder.btnPlayAgain.visibility =  View.INVISIBLE
        }

        holder.btnPlayAgain
    }

    override fun getItemCount() = userDb.size
}