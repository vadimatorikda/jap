package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class MenuTraining : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_menu_training, container, false)

        v.findViewById<Button>(R.id.btnMemoryCards).setOnClickListener {
            actInterface?.callNextFragment("MenuMemoryCards")
        }
        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("MainMenu")
        }
        return v
    }
}