package jap.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class Start : BaseFragment() {
    private lateinit var ct: Context
    private lateinit var btnStart: Button
    private lateinit var btnExit: Button
    private var ws = WaitStatus()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_start, container, false)
        ct = v.context
        val fm = childFragmentManager.beginTransaction()
        fm.add(R.id.status_container, ws).hide(ws).commit()
        btnStart = v.findViewById(R.id.btnStart)
        btnStart.setOnClickListener {
            lockScreen(true)
            ws.setText(resources.getString(R.string.tvMsgStart))
            showStatusFragment()
            Thread { start() }.start()
        }
        btnExit = v.findViewById(R.id.btnExit)
        btnExit.setOnClickListener {
            actInterface?.callNextFragment("Exit")
        }
        return v
    }

    private fun showStatusFragment() {
        val fm = childFragmentManager.beginTransaction()
        fm.show(ws).commit()
    }

    private fun hideStatusFragment() {
        val fm = childFragmentManager.beginTransaction()
        fm.hide(ws).commit()
    }

    private fun lockScreen(lock: Boolean) {
        btnStart.isClickable = !lock
        btnExit.isClickable = !lock
    }

    private fun start() {
        if (FsClient.isUserProfileExist(ct)) {
            enter()
        } else {
            reg()
        }
    }

    private fun enter() {
        val cfg = SettingsClient(ct)
        do {
            val commonDatabase = getCommonDatabase(cfg)
            if (!FsClient.setCommonDatabase(ct, commonDatabase)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgErrAccessToInternalFlash))
                break
            }
            if (!DatabaseClient.initCommonDatabase(commonDatabase)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgCommonDictionaryCorrupted))
                break
            }
            val userProgress = FsClient.getUserProgress(ct)
            if (userProgress.isEmpty()) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgErrAccessToInternalFlash))
                break
            }
            DatabaseClient.resetUserDatabase()
            if (!DatabaseClient.initUserProgress(userProgress)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgUserProgressCorrupted))
                break
            }
            checkStorageAudioFiles()
            upgradeAudioFiles(cfg)
            printErrMsgFromThread(String())
            actInterface?.callNextFragment("MainMenu")
        } while (false)
        lockScreen(false)
        hideStatusFragment()
    }

    private fun reg() {
        do {
            if (!FsClient.createProfile(ct)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgErrAccessToInternalFlash))
                break
            }
            var commonDatabase: String
            do {
                commonDatabase = getCommonDatabaseFromServer()
                if (commonDatabase.isEmpty()) {
                    showReplayOnlyDialog(
                        resources.getString(R.string.adMsgErrEthConnect),
                        resources.getString(R.string.btnReplay)
                    )
                    continue
                }
                break
            } while (true)
            if (!FsClient.setCommonDatabase(ct, commonDatabase)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgErrAccessToInternalFlash))
                break
            }
            if (!DatabaseClient.initCommonDatabase(commonDatabase)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgCommonDictionaryCorrupted))
                break
            }
            DatabaseClient.resetUserDatabase()
            val emptyUserProgress = DatabaseClient.getEmptyUserProgress()
            if (!FsClient.setUserProgress(ct, emptyUserProgress)) {
                printErrMsgFromThread(resources.getString(R.string.tvMsgErrAccessToInternalFlash))
                break
            }
            printErrMsgFromThread(String())
            actInterface?.callNextFragment("MainMenu")
        } while (false)
        lockScreen(false)
        hideStatusFragment()
    }

    private fun getCommonDatabaseFromServer(): String {
        if (!FtpsClient.login()) {
            return String()
        }
        val commonDatabase = FtpsClient.getCommonDatabase()
        FtpsClient.out()
        return commonDatabase
    }

    private fun getCommonDatabase(cfg: SettingsClient): String {
        return if (cfg.getEveryLoginUpgradeCommonDictionaryState()) {
            getComDbFromServer()
        } else {
            getComDbFromStorage()
        }
    }

    private fun getComDbFromServer(): String {
        do {
            do {
                val commonDatabase = getCommonDatabaseFromServer()
                if (commonDatabase.isNotEmpty()) {
                    return commonDatabase
                }
                if (isGetReplay(
                        resources.getString(R.string.adMsgErrEthConnect),
                        resources.getString(R.string.btnReplay),
                        resources.getString(R.string.btnNextWithoutDownload)
                    )
                ) {
                    continue
                }
                break
            } while (true)
            do {
                val commonDatabase = FsClient.getCommonDatabase(ct)
                if (commonDatabase.isNotEmpty()) {
                    return commonDatabase
                }
                if (isGetReplay(
                        resources.getString(R.string.adMsgErrOpenCommonDatabase),
                        resources.getString(R.string.btnReplayOpen),
                        resources.getString(R.string.btnDownloadFromServer)
                    )
                ) {
                    continue
                }
                break
            } while (true)
        } while (true)
    }

    private fun checkStorageAudioFiles() {
        printErrMsgFromThread(resources.getString(R.string.tvMsgIndexingAudioInDevice))
        while (!AudioClient.initFsAudioList(ct)) {
            if (isGetReplay(
                    resources.getString(R.string.adMsgErrIndexingAudio),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutAudio)
                )
            ) {
                continue
            } else {
                break
            }
        }
    }

    private fun upgradeAudioFiles(cfg: SettingsClient) {
        if (!cfg.getEveryLoginUpgradeAudioForUserState()) {
            return
        }
        printErrMsgFromThread(resources.getString(R.string.tvMsgUpgradeAudioInDevice))
        val serverJpList = DatabaseClient.getUserJpList()
        while (!FtpsClient.login() || !AudioClient.upgradeAudioFiles(
                ct,
                serverJpList,
                this::onPbFromThread,
                this::setPercentPbFromThread
            )
        ) {
            FtpsClient.out()
            if (isGetReplay(
                    resources.getString(R.string.adMsgErrEthConnect),
                    resources.getString(R.string.btnReplay),
                    resources.getString(R.string.btnNextWithoutDownload)
                )
            ) {
                continue
            } else {
                break
            }
        }
        FtpsClient.out()
    }


    private fun printErrMsgFromThread(msg: String) {
        activity?.runOnUiThread {
            ws.setText(msg)
        }
    }

    private fun setPercentPbFromThread(percent: Int) {
        activity?.runOnUiThread {
            ws.setProgressPercent(percent)
        }
    }

    private fun onPbFromThread() {
        activity?.runOnUiThread {
            hideStatusFragment()
        }
    }

    private fun getComDbFromStorage(): String {
        do {
            do {
                val commonDatabase = FsClient.getCommonDatabase(ct)
                if (commonDatabase.isNotEmpty()) {
                    return commonDatabase
                }
                if (isGetReplay(
                        resources.getString(R.string.adMsgErrOpenCommonDatabase),
                        resources.getString(R.string.btnReplayOpen),
                        resources.getString(R.string.btnDownloadFromServer)
                    )
                ) {
                    continue
                }
                break
            } while (true)
            do {
                val commonDatabase = getCommonDatabaseFromServer()
                if (commonDatabase.isNotEmpty()) {
                    return commonDatabase
                }
                if (isGetReplay(
                        resources.getString(R.string.adMsgErrEthConnect),
                        resources.getString(R.string.btnReplay),
                        resources.getString(R.string.btnNextWithoutDownload)
                    )
                ) {
                    continue
                }
                break
            } while (true)
        } while (true)
    }
}
