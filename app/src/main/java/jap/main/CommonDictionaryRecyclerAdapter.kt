package jap.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding

class CommonDictionaryRecyclerAdapter(
    private val context: Context,
    tag: String,
    type: DatabaseClient.SortType,
    private val checkWordStateChange: (id: Int) -> Unit
) :
    RecyclerView.Adapter<CommonDictionaryRecyclerAdapter.MyViewHolder>() {
    private val commonDb = (if (tag.isEmpty()) {
        DatabaseClient.getSortCommonDatabase(String(), type)
    } else {
        DatabaseClient.getSortCommonDatabase(tag, type)
    })

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cb: CheckBox = itemView.findViewById(R.id.cb)
        val tv: TextView = itemView.findViewById(R.id.tv)
        val btnPlayAgain: Button = itemView.findViewById(R.id.btnPlayAgain)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.common_dictionary_recyclerview_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val i = commonDb[position]
        var str = i.jp + '\n'
        if (i.kanji.isNotEmpty()) {
            str += i.kanji + '\n'
        }
        var isFirst = true
        for (ruItem in i.ru) {
            if (!isFirst) {
                str += '\n'
            } else {
                isFirst = false
            }
            str += ruItem
        }
        holder.tv.text = str
        holder.cb.setOnCheckedChangeListener(null)
        holder.cb.isChecked = DatabaseClient.isUserContainsWord(i.id)
        holder.cb.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                checkWordStateChange(i.id)
                DatabaseClient.addWordToUserDatabase(i.id)
            } else {
                checkWordStateChange(i.id)
                DatabaseClient.removeWordFromUserDatabase(i.id)
            }
        }

        holder.btnPlayAgain.setOnClickListener(null)
        if (AudioClient.isAudioExist(i.jp)) {
            holder.btnPlayAgain.setOnClickListener {
                AudioClient.playAudio(context, i.jp)
            }
            holder.btnPlayAgain.visibility = View.VISIBLE
        } else {
            holder.btnPlayAgain.visibility =  View.INVISIBLE
        }
    }

    override fun getItemCount() = commonDb.size
}