package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox

class SettingsOptions : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_settings_options, container, false)
        val ct = v.context

        val cfg = SettingsClient(ct)

        val cbPlayAudio = v.findViewById<CheckBox>(R.id.cbPlayAudio)
        cbPlayAudio.isChecked =
            cfg.getAudioPlayingState()
        cbPlayAudio.setOnCheckedChangeListener { _, isChecked ->
            cfg.setAudioPlayingState(isChecked)
        }

        val cbEveryLoginUpgradeCommonDictionary =
            v.findViewById<CheckBox>(R.id.cbEveryLoginUpgradeCommonDictionary)
        cbEveryLoginUpgradeCommonDictionary.isChecked =
            cfg.getEveryLoginUpgradeCommonDictionaryState()
        cbEveryLoginUpgradeCommonDictionary.setOnCheckedChangeListener { _, isChecked ->
            cfg.setEveryLoginUpgradeCommonDictionaryState(isChecked)
        }

        val cbEveryLoginUpgradeAudioForUserDictionary =
            v.findViewById<CheckBox>(R.id.cbEveryLoginUpgradeAudioForUserDictionary)
        cbEveryLoginUpgradeAudioForUserDictionary.isChecked =
            cfg.getEveryLoginUpgradeAudioForUserState()
        cbEveryLoginUpgradeAudioForUserDictionary.setOnCheckedChangeListener { _, isChecked ->
            cfg.setEveryLoginUpgradeAudioForUserState(isChecked)
        }

        val cbAutoDownloadAudioForUserDictionaryAfterReturn =
            v.findViewById<CheckBox>(R.id.cbAutoDownloadAudioForUserDictionaryAfterReturn)
        cbAutoDownloadAudioForUserDictionaryAfterReturn.isChecked =
            cfg.getAutoDownloadAudioForUserDictionaryAfterState()
        cbAutoDownloadAudioForUserDictionaryAfterReturn.setOnCheckedChangeListener { _, isChecked ->
            cfg.setAutoDownloadAudioForUserDictionaryAfterState(isChecked)
        }

        val cbAutoRemoveUnusedAudioByUserDictionaryAfterReturn =
            v.findViewById<CheckBox>(R.id.cbAutoRemoveUnusedAudioByUserDictionaryAfterReturn)
        cbAutoRemoveUnusedAudioByUserDictionaryAfterReturn.isChecked =
            cfg.getAutoRemoveUnusedAudioByUserDictionaryAfterReturnState()
        cbAutoRemoveUnusedAudioByUserDictionaryAfterReturn.setOnCheckedChangeListener { _, isChecked ->
            cfg.setAutoRemoveUnusedAudioByUserDictionaryAfterReturnState(isChecked)
        }
        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment("Settings")
        }

        return v
    }
}
