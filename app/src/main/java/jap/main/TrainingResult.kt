package jap.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class TrainingResult : BaseFragment() {
    private lateinit var rvKnow: RecyclerView
    private lateinit var rvDonTKnow: RecyclerView

    private lateinit var llKnow: LinearLayout
    private lateinit var llDonTKnow: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v = inflater.inflate(R.layout.fragment_training_result, container, false)
        val ct = v.context

        rvKnow = v.findViewById(R.id.rvKnow)
        rvDonTKnow = v.findViewById(R.id.rvDonTKnow)
        llKnow = v.findViewById(R.id.llKnow)
        llDonTKnow = v.findViewById(R.id.llDonTKnow)

        val llm = LinearLayoutManager(ct);
        val dividerItemDecoration = DividerItemDecoration(
            rvKnow.context,
            llm.orientation
        )

        val knowFirst = arguments?.getStringArrayList("know_first") ?: arrayListOf<String>()
        val knowSecond = arguments?.getStringArrayList("know_second") ?: arrayListOf<String>()
        if (knowFirst.isNotEmpty()) {
            rvKnow.layoutManager = LinearLayoutManager(ct)
            rvKnow.addItemDecoration(dividerItemDecoration)
            rvKnow.adapter = TrainingResultRecyclerAdapter(knowFirst, knowSecond)
        } else {
            llKnow.visibility = View.GONE
        }

        val donTKnowFirst =
            arguments?.getStringArrayList("don_t_know_first") ?: arrayListOf<String>()
        val donTKnowSecond =
            arguments?.getStringArrayList("don_t_know_second") ?: arrayListOf<String>()
        if (donTKnowFirst.isNotEmpty()) {
            rvDonTKnow.layoutManager = LinearLayoutManager(ct)
            rvDonTKnow.addItemDecoration(dividerItemDecoration)
            rvDonTKnow.adapter =
                TrainingResultRecyclerAdapter(donTKnowFirst, donTKnowSecond)
        } else {
            llDonTKnow.visibility = View.GONE
        }

        v.findViewById<Button>(R.id.btnReturn).setOnClickListener {
            actInterface?.callNextFragment(
                arguments?.getString("return_fragment_name") ?: "TrainingResult"
            )
        }

        return v
    }
}