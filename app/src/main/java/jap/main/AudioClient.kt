package jap.main

import android.content.Context
import android.media.MediaPlayer
import java.util.*
import kotlin.reflect.KFunction1

object AudioClient {
    private var fsAudioFileList = Vector<String>()

    fun initFsAudioList(context: Context): Boolean {
        fsAudioFileList.clear()
        val fsAudioList = FsClient.getAudioList(context) ?: return false
        for (i in fsAudioList) {
            if (i == null) {
                continue
            }
            fsAudioFileList.addElement(i)
        }
        return true
    }

    fun upgradeAudioFiles(
        context: Context,
        jp: Vector<String>,
        onProgressBar: () -> Unit?,
        updateProgressBar: (Int) -> Unit?
    ): Boolean {
        val ftpAudioList = FtpsClient.getAudioList()
        val downloadFileList = Vector<String>()
        for (i in jp) {
            val fileName = "$i.wav"
            if (!fsAudioFileList.contains(fileName)) {
                if (ftpAudioList.contains(fileName)) {
                    downloadFileList.addElement(fileName)
                } else {
                    continue
                }
            }
            var suffix = 1
            do {
                val fileNameWithSuffix = i + "_" + suffix.toString() + ".wav"
                if (!fsAudioFileList.contains(fileNameWithSuffix)) {
                    if (ftpAudioList.contains(fileNameWithSuffix)) {
                        downloadFileList.addElement(fileNameWithSuffix)
                    } else {
                        break
                    }
                }
                suffix++
            } while (true)
        }
        if (downloadFileList.isEmpty()) {
            return true
        }
        val fNum = downloadFileList.size
        var fCounter = 0
        onProgressBar()
        updateProgressBar(0)
        for (i in downloadFileList) {
            val fileData = FtpsClient.getAudioFile(i)
            if (fileData.isEmpty()) {
                return false
            }
            if (!FsClient.setAudioFile(context, i, fileData)) {
                return false
            }
            fsAudioFileList.addElement(i)
            fCounter++
            updateProgressBar((100.0F / fNum * fCounter).toInt())
        }
        return true
    }

    fun removeAudioFiles(
        context: Context,
        jp: Vector<String>,
        onProgressBar: () -> Unit?,
        updateProgressBar: (Int) -> Unit?
    ): Boolean {
        val removeFileList = Vector<String>()
        for (i in fsAudioFileList) {
            if (i.contains("_")) {
                continue
            }
            val jpItem = i.replace(".wav", "")
            if (jp.contains(jpItem)) {
                continue
            }
            removeFileList.addElement(i)
            val jpWithSuffixFileList = getJpFilesNameVector(fsAudioFileList, jpItem)
            removeFileList += jpWithSuffixFileList
        }
        if (removeFileList.isEmpty()) {
            return true
        }
        val fNum = removeFileList.size
        var fCounter = 0
        onProgressBar()
        updateProgressBar(0)
        for (i in removeFileList) {
            if (!FsClient.removeAudioFile(context, i)) {
                return false
            }
            fsAudioFileList.removeElement(i)
            fCounter++
            updateProgressBar((100.0F / fNum * fCounter).toInt())
        }
        return true
    }

    fun removeAudioCash(context: Context): Boolean {
        fsAudioFileList.clear()
        return FsClient.clearAudioDir(context)
    }

    private fun getJpFilesNameVector(fileNames: Vector<String>, jp: String): Vector<String> {
        val rv = Vector<String>()
        for (i in fileNames) {
            if (i.contains(jp + "_")) {
                rv.addElement(i)
            }
        }
        return rv
    }

    fun isAudioExist(jp: String): Boolean {
        return fsAudioFileList.contains("$jp.wav")
    }

    fun playAudio(context: Context, jp: String) {
        if (!SettingsClient(context).getAudioPlayingState()) {
            return
        }
        val mp = MediaPlayer()
        val variantNumber = getAudioVariantNumber(jp)
        if (variantNumber == 0) {
            return
        }
        val topRange = variantNumber - 1
        val i = (0..topRange).random()
        val path = if (i == 0) {
            context.filesDir.absolutePath + "/audio/jp/$jp.wav"
        } else {
            context.filesDir.absolutePath + "/audio/jp/" + jp + "_" + i.toString() + ".wav"
        }
        try {
            mp.setDataSource(path)
            mp.prepare()
            mp.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getAudioVariantNumber(jp: String): Int {
        if (!isAudioExist(jp)) {
            return 0
        }
        var num = 1
        while (true) {
            if (!fsAudioFileList.contains(jp + "_" + num.toString() + ".wav")) {
                return num // +1, first audio without number.
            }
            num++
        }
    }
}