package jap.main

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import java.util.concurrent.Semaphore

open class BaseFragment : Fragment() {
    interface Interface {
        fun hideSystemUI()
        fun callNextFragment(name: String)
        fun callNextFragment(name: String, params: Bundle)
    }

    protected var actInterface: Interface? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Interface) {
            actInterface = context
        }
    }

    protected fun showReplayOnlyDialog(title: String, ok: String) {
        val sem = Semaphore(0)
        activity?.runOnUiThread {
            val b = AlertDialog.Builder(activity)
            b.setTitle(title)
            b.setPositiveButton(
                ok
            ) { _, _ ->
                sem.release()
            }
            b.setOnCancelListener {
                sem.release()
            }
            b.show()
        }
        sem.acquire()
        activity?.runOnUiThread {
            actInterface?.hideSystemUI()
        }
    }

    protected fun isGetReplay(title: String, ok: String, no: String): Boolean {
        val sem = Semaphore(0)
        var replay = true
        activity?.runOnUiThread {
            val b = AlertDialog.Builder(activity)

            b.setTitle(title)
            b.setPositiveButton(
                ok
            ) { _, _ ->
                sem.release()
            }
            b.setNegativeButton(
                no
            ) { _, _ ->
                replay = false
                sem.release()
            }
            b.setOnCancelListener {
                sem.release()
            }
            b.show()
        }
        sem.acquire()
        activity?.runOnUiThread {
            actInterface?.hideSystemUI()
        }
        return replay
    }

    protected fun showOkMsg(title: String) {
        val sem = Semaphore(0)
        activity?.runOnUiThread {
            val b = AlertDialog.Builder(activity)
            b.setMessage(title)
            b.setPositiveButton(
                resources.getString(R.string.btnOk)
            ) { _, _ ->
                sem.release()
            }
            b.setOnCancelListener {
                sem.release()
            }
            b.show()
        }
        sem.acquire()
        activity?.runOnUiThread {
            actInterface?.hideSystemUI()
        }
    }
}