package jap.main

import kotlinx.datetime.Clock
import java.util.*


object DatabaseClient {
    data class CommonItem(
        var id: Int = 0,
        var jp: String = String(),
        var kanji: String = String(),
        var tags: Vector<String> = Vector<String>(),
        var ru: Vector<String> = Vector<String>()
    )

    enum class StatisticAction(val value: Int) {
        None(0),
        JpToRuCounterUserDec(1),
        RuToJpCounterUserDec(2),
        KanjiToJpCounterUserDec(3),
        JpToKanjiCounterUserDec(4),
        KanjiToRuCounterUserDec(5),
        RuToKanjiCounterUserDec(6),
        JpToRuCounterUserInc(7),
        RuToJpCounterUserInc(8),
        KanjiToJpCounterUserInc(9),
        JpToKanjiCounterUserInc(10),
        KanjiToRuCounterUserInc(11),
        RuToKanjiCounterUserInc(12),
        JpToRuCounterSystemDec(13),
        RuToJpCounterSystemDec(14),
        KanjiToJpCounterSystemDec(15),
        JpToKanjiCounterSystemDec(16),
        KanjiToRuCounterSystemDec(17),
        RuToKanjiCounterSystemDec(18);

        companion object {
            fun fromInt(value: Int) = values().first { it.value == value }
        }
    }

    data class StatItem(
        var time: String = String(),
        var act: StatisticAction = StatisticAction.None,
        var value: Int = 0
    )

    data class UserItem(
        var id: Int = 0,
        var jpToRuCounter: Int = 0,
        var ruToJpCounter: Int = 0,
        var kanjiToJpCounter: Int = 0,
        var jpToKanjiCounter: Int = 0,
        var kanjiToRuCounter: Int = 0,
        var ruToKanjiCounter: Int = 0,
        var statistic: Vector<StatItem> = Vector<StatItem>()
    )

    enum class Types(val value: Int) {
        JpToRu(0),
        RuToJp(1),
        KanjiToJp(2),
        JpToKanji(3),
        KanjiToRu(4),
        RuToKanji(5);

        companion object {
            fun fromInt(value: Int) = values().first { it.value == value }
        }
    }

    enum class SortType(val value: Int) {
        NewToOld(0),
        OldToNew(1),
        AToO(2),
        OToA(3),
        JpToRuUp(4),
        RuToJpUp(5),
        KanjiToJpUp(6),
        JpToKanjiUp(7),
        KanjiToRuUp(8),
        RuToKanjiUp(9),
        JpToRuDown(10),
        RuToJpDown(11),
        KanjiToJpDown(12),
        JpToKanjiDown(13),
        KanjiToRuDown(14),
        RuToKanjiDown(15);

        companion object {
            fun fromInt(value: Int) = values().first { it.value == value }
        }
    }

    fun resetUserDatabase() {
        userItems_.clear()
    }

    fun getEmptyUserProgress(): String {
        var str = String()
        for (i in userProgressHeaderItems.indices) {
            str += userProgressHeaderItems[i]
            if (i != userProgressHeaderItems.size - 1) {
                str += '\t'
            }
        }
        return str
    }

    fun initCommonDatabase(fileData: String): Boolean {
        val lines = fileData.split('\n')
        var isData = false
        for (line in lines) {
            if (isData) {
                if (line.isNotEmpty() && !parseCommonDatabaseLine(line)) {
                    return false
                }
            } else {
                if (!isDbHeaderValid(line)) {
                    return false
                }
                isData = true
            }
        }
        return true
    }

    fun initUserProgress(fileData: String): Boolean {
        val lines = fileData.split('\n')
        var isData = false
        for (line in lines) {
            if (isData) {
                if (line.isNotEmpty() && !parseUserDatabaseLine(line)) {
                    return false
                }
            } else {
                if (!isUserProgressHeaderValid(line)) {
                    return false
                }
                isData = true
            }
        }
        return true
    }

    fun getCurrentProgress(): String {
        var str = String()
        for (i in userProgressHeaderItems.indices) {
            str += userProgressHeaderItems[i]
            if (i != userProgressHeaderItems.size - 1) {
                str += '\t'
            }
        }
        for ((_, i) in userItems_) {
            str += '\n'
            str += i.id.toString() + '\t'
            str += i.jpToRuCounter.toString() + '\t'
            str += i.ruToJpCounter.toString() + '\t'
            str += i.kanjiToJpCounter.toString() + '\t'
            str += i.jpToKanjiCounter.toString() + '\t'
            str += i.kanjiToRuCounter.toString() + '\t'
            str += i.ruToKanjiCounter.toString() + '\t'
            str += getStatStr(i.statistic)
        }
        return str
    }

    fun getUserJpList(): Vector<String> {
        val v: Vector<String> = Vector()
        for ((key, _) in userItems_) {
            val item = commonItems_[key] ?: continue
            if (!v.contains(item.jp)) {
                v.addElement(item.jp)
            }
        }
        return v
    }

    fun getCommonJpList(): Vector<String> {
        val v: Vector<String> = Vector()
        for (i in commonItems_) {
            v.addElement(i.value.jp)
        }
        return v
    }

    fun getCommonDatabaseTags(): Vector<String> {
        val v: Vector<String> = Vector()
        for ((_, item) in commonItems_) {
            for (value in item.tags) {
                if (!v.contains(value)) {
                    v.addElement(value)
                }
            }
        }
        v.sort()
        return v
    }

    fun getUserDatabaseTags(): Vector<String> {
        val v: Vector<String> = Vector()
        for ((key, _) in userItems_) {
            for (value in commonItems_[key]?.tags!!) {
                if (!v.contains(value)) {
                    v.addElement(value)
                }
            }
        }
        v.sort()
        return v
    }

    fun getSortCommonDatabase(tag: String, type: SortType): Vector<CommonItem> {
        val v = Vector<String>()
        if (tag.isNotEmpty()) {
            v.addElement(tag)
        }
        return getSortCommonDatabase(v, type)
    }

    fun isUserContainsWord(id: Int): Boolean {
        return userItems_.contains(id)
    }

    fun addWordToUserDatabase(id: Int) {
        if (isUserContainsWord(id)) {
            return
        }
        val o = UserItem(id)
        userItems_[id] = o
    }

    fun removeWordFromUserDatabase(id: Int) {
        if (isUserContainsWord(id)) {
            userItems_.remove(id)
        }
    }

    fun getSortCommonDatabaseForUser(tag: String, type: SortType): Vector<CommonItem> {
        val v = Vector<String>()
        if (tag.isNotEmpty()) {
            v.addElement(tag)
        }
        return getSortCommonDatabaseForUser(v, type)
    }

    fun getUserDatabaseItem(id: Int): UserItem {
        return userItems_[id] ?: return UserItem()
    }

    fun getSortUserDatabase(t: Types, tag: String): Vector<UserItem> {
        val v = Vector<String>()
        if (tag.isNotEmpty()) {
            v.addElement(tag)
        }
        return getSortUserDatabase(t, v)
    }

    fun getSortUserDatabase(t: Types, tags: Vector<String>): Vector<UserItem> {
        val items = Vector<UserItem>()
        for ((_, item) in userItems_) {
            items.addElement(item)
        }
        removeNotNeedItems(items, t, tags)
        when (t) {
            Types.JpToRu -> {
                items.sortBy { it.jpToRuCounter }
            }
            Types.RuToJp -> {
                items.sortBy { it.ruToJpCounter }
            }
            Types.KanjiToJp -> {
                items.sortBy { it.kanjiToJpCounter }
            }
            Types.JpToKanji -> {
                items.sortBy { it.jpToKanjiCounter }
            }
            Types.KanjiToRu -> {
                items.sortBy { it.kanjiToRuCounter }
            }
            Types.RuToKanji -> {
                items.sortBy { it.ruToKanjiCounter }
            }
        }
        return items
    }

    fun getCommonDatabaseItem(id: Int): CommonItem {
        return commonItems_[id] ?: return CommonItem()
    }

    fun incItem(t: Types, id: Int) {
        val item = userItems_[id] ?: return
        when (t) {
            Types.JpToRu -> {
                if (item.jpToRuCounter < 100) item.jpToRuCounter++
                addStatisticItem(id, StatisticAction.JpToRuCounterUserInc, 1)
            }
            Types.RuToJp -> {
                if (item.ruToJpCounter < 100) item.ruToJpCounter++
                addStatisticItem(id, StatisticAction.RuToJpCounterUserInc, 1)
            }
            Types.KanjiToJp -> {
                if (item.kanjiToJpCounter < 100) item.kanjiToJpCounter++
                addStatisticItem(id, StatisticAction.KanjiToJpCounterUserInc, 1)
            }
            Types.JpToKanji -> {
                if (item.jpToKanjiCounter < 100) item.jpToKanjiCounter++
                addStatisticItem(id, StatisticAction.JpToKanjiCounterUserInc, 1)
            }
            Types.KanjiToRu -> {
                if (item.kanjiToRuCounter < 100) item.kanjiToRuCounter++
                addStatisticItem(id, StatisticAction.KanjiToRuCounterUserInc, 1)
            }
            Types.RuToKanji -> {
                if (item.ruToKanjiCounter < 100) item.ruToKanjiCounter++
                addStatisticItem(id, StatisticAction.RuToKanjiCounterUserInc, 1)
            }
        }
    }

    fun decItem(t: Types, id: Int) {
        val item = userItems_[id] ?: return
        when (t) {
            Types.JpToRu -> {
                if (item.jpToRuCounter > 0) item.jpToRuCounter--
                addStatisticItem(id, StatisticAction.JpToRuCounterUserDec, 1)
            }
            Types.RuToJp -> {
                if (item.ruToJpCounter > 0) item.ruToJpCounter--
                addStatisticItem(id, StatisticAction.RuToJpCounterUserDec, 1)
            }
            Types.KanjiToJp -> {
                if (item.kanjiToJpCounter > 0) item.kanjiToJpCounter--
                addStatisticItem(id, StatisticAction.KanjiToJpCounterUserDec, 1)
            }
            Types.JpToKanji -> {
                if (item.jpToKanjiCounter > 0) item.jpToKanjiCounter--
                addStatisticItem(id, StatisticAction.JpToKanjiCounterUserDec, 1)
            }
            Types.KanjiToRu -> {
                if (item.kanjiToRuCounter > 0) item.kanjiToRuCounter--
                addStatisticItem(id, StatisticAction.KanjiToRuCounterUserDec, 1)
            }
            Types.RuToKanji -> {
                if (item.ruToKanjiCounter > 0) item.ruToKanjiCounter--
                addStatisticItem(id, StatisticAction.RuToKanjiCounterUserDec, 1)
            }
        }
    }

    private fun addStatisticItem(id: Int, act: StatisticAction, value: Int) {
        val userItem = userItems_[id] ?: return
        val statItem = StatItem(Clock.System.now().toString(), act, value)
        userItem.statistic.addElement(statItem)
    }

    private fun parseCommonDatabaseLine(line: String): Boolean {
        val o = CommonItem()
        val itemFields = line.split('\t')
        if (itemFields.size != ITEM_DB_NUM) {
            return false
        }
        o.id = itemFields[ItemDbDefPos.Id.value].toInt()
        o.jp = itemFields[ItemDbDefPos.Jp.value]
        o.kanji = itemFields[ItemDbDefPos.Kanji.value]
        if (!itemFields[ItemDbDefPos.Tags.value].isEmpty()) {
            for (i in itemFields[ItemDbDefPos.Tags.value].split(",")) {
                o.tags.addElement(i)
            }
        }
        if (!itemFields[ItemDbDefPos.Ru.value].isEmpty()) {
            for (i in itemFields[ItemDbDefPos.Ru.value].split(",")) {
                o.ru.addElement(i)
            }
        }
        commonItems_[o.id] = o
        return true
    }

    private fun getStatStr(stat: Vector<StatItem>): String {
        var firstItem = true
        var str = String()
        for (i in stat) {
            if (!firstItem) {
                str += ','
            } else {
                firstItem = false
            }
            str += i.time + ';' + i.act.value.toString() + ';' + i.value
        }
        return str
    }

    private fun getStatisticFromStr(strStat: String): Vector<StatItem> {
        val statItems = strStat.split(',')
        val v = Vector<StatItem>()
        for (i in statItems) {
            val statItem = i.split(';')
            if (statItem.size != 3) {
                return Vector<StatItem>()
            }
            v.addElement(
                StatItem(
                    statItem[0],
                    StatisticAction.fromInt(statItem[1].toInt()),
                    statItem[2].toInt()
                )
            )
        }
        return v
    }

    private fun isDbHeaderValid(line: String): Boolean {
        val items = line.split('\t')
        return items.size == ITEM_DB_NUM
    }

    private fun parseUserDatabaseLine(line: String): Boolean {
        val o = UserItem()
        val itemFields = line.split('\t')
        if (itemFields.size != ITEM_USER_PROGRESS_NUM) {
            return false
        }

        val id = itemFields[ItemUserProgressPos.Id.value].toInt()
        if (!commonItems_.contains(id)) {
            return true
        }

        o.id = id
        o.jpToRuCounter = itemFields[ItemUserProgressPos.JpToRuCounter.value].toInt()
        o.ruToJpCounter = itemFields[ItemUserProgressPos.RuToJpCounter.value].toInt()
        o.kanjiToJpCounter = itemFields[ItemUserProgressPos.KanjiToJpCounter.value].toInt()
        o.jpToKanjiCounter = itemFields[ItemUserProgressPos.JpToKanjiCounter.value].toInt()
        o.kanjiToRuCounter = itemFields[ItemUserProgressPos.KanjiToRuCounter.value].toInt()
        o.ruToKanjiCounter = itemFields[ItemUserProgressPos.RuToKanjiCounter.value].toInt()
        o.statistic = getStatisticFromStr(itemFields[ItemUserProgressPos.Date.value])
        userItems_[id] = o

        return true
    }

    private fun isUserProgressHeaderValid(line: String): Boolean {
        val items = line.split('\t')
        return items.size == ITEM_USER_PROGRESS_NUM
    }

    private fun getSortCommonDatabase(tags: Vector<String>, type: SortType): Vector<CommonItem> {
        val items = Vector<CommonItem>()
        for ((_, item) in commonItems_) {
            items.addElement(item)
        }
        removeNotNeedItems(items, tags)
        when (type) {
            SortType.NewToOld -> {
                items.sortByDescending { it.id }
            }
            SortType.OldToNew -> {
                items.sortBy { it.id }
            }
            SortType.AToO -> {
                items.sortBy { it.jp }
            }
            SortType.OToA -> {
                items.sortByDescending { it.jp }
            }
            SortType.JpToRuUp -> {
                items.sortBy { userItems_[it.id]?.jpToRuCounter }
            }
            SortType.RuToJpUp -> {
                items.sortBy { userItems_[it.id]?.ruToJpCounter }
            }
            SortType.KanjiToJpUp -> {
                items.sortBy { userItems_[it.id]?.kanjiToJpCounter }
            }
            SortType.JpToKanjiUp -> {
                items.sortBy { userItems_[it.id]?.jpToKanjiCounter }
            }
            SortType.KanjiToRuUp -> {
                items.sortBy { userItems_[it.id]?.kanjiToRuCounter }
            }
            SortType.RuToKanjiUp -> {
                items.sortBy { userItems_[it.id]?.ruToKanjiCounter }
            }
            SortType.JpToRuDown -> {
                items.sortByDescending { userItems_[it.id]?.jpToRuCounter }
            }
            SortType.RuToJpDown -> {
                items.sortByDescending { userItems_[it.id]?.ruToJpCounter }
            }
            SortType.KanjiToJpDown -> {
                items.sortByDescending { userItems_[it.id]?.kanjiToJpCounter }
            }
            SortType.JpToKanjiDown -> {
                items.sortByDescending { userItems_[it.id]?.jpToKanjiCounter }
            }
            SortType.KanjiToRuDown -> {
                items.sortByDescending { userItems_[it.id]?.kanjiToRuCounter }
            }
            SortType.RuToKanjiDown -> {
                items.sortByDescending { userItems_[it.id]?.ruToKanjiCounter }
            }
        }
        return items
    }

    private fun getSortCommonDatabaseForUser(
        tags: Vector<String>,
        type: SortType
    ): Vector<CommonItem> {
        val items = Vector<CommonItem>()
        for ((_, item) in commonItems_) {
            items.addElement(item)
        }
        removeNotNeedItems(items, tags)
        for (i in items.size - 1 downTo 0) {
            if (!isUserContainsWord(items[i].id)) {
                items.removeElementAt(i)
            }
        }
        when (type) {
            SortType.NewToOld -> {
                items.sortByDescending { it.id }
            }
            SortType.OldToNew -> {
                items.sortBy { it.id }
            }
            SortType.AToO -> {
                items.sortBy { it.jp }
            }
            SortType.OToA -> {
                items.sortByDescending { it.jp }
            }
            SortType.JpToRuUp -> {
                items.sortBy { userItems_[it.id]?.jpToRuCounter }
            }
            SortType.RuToJpUp -> {
                items.sortBy { userItems_[it.id]?.ruToJpCounter }
            }
            SortType.KanjiToJpUp -> {
                items.sortBy { userItems_[it.id]?.kanjiToJpCounter }
            }
            SortType.JpToKanjiUp -> {
                items.sortBy { userItems_[it.id]?.jpToKanjiCounter }
            }
            SortType.KanjiToRuUp -> {
                items.sortBy { userItems_[it.id]?.kanjiToRuCounter }
            }
            SortType.RuToKanjiUp -> {
                items.sortBy { userItems_[it.id]?.ruToKanjiCounter }
            }
            SortType.JpToRuDown -> {
                items.sortByDescending { userItems_[it.id]?.jpToRuCounter }
            }
            SortType.RuToJpDown -> {
                items.sortByDescending { userItems_[it.id]?.ruToJpCounter }
            }
            SortType.KanjiToJpDown -> {
                items.sortByDescending { userItems_[it.id]?.kanjiToJpCounter }
            }
            SortType.JpToKanjiDown -> {
                items.sortByDescending { userItems_[it.id]?.jpToKanjiCounter }
            }
            SortType.KanjiToRuDown -> {
                items.sortByDescending { userItems_[it.id]?.kanjiToRuCounter }
            }
            SortType.RuToKanjiDown -> {
                items.sortByDescending { userItems_[it.id]?.ruToKanjiCounter }
            }
        }
        return items
    }

    private fun removeNotNeedItems(v: Vector<UserItem>, t: Types, tags: Vector<String>) {
        var curItem = v.size - 1
        while (curItem >= 0) {
            val id = v[curItem].id
            val item = commonItems_[id]
            if (item != null) {
                when (t) {
                    Types.JpToRu, Types.RuToJp -> {
                        if (item.ru.isEmpty() || item.jp.isEmpty() ||
                            (!isContainsTags(item.tags, tags))
                        ) {
                            v.removeElementAt(curItem)
                        }
                    }
                    Types.KanjiToJp, Types.JpToKanji -> {
                        if (item.jp.isEmpty() || item.kanji.isEmpty() ||
                            (!isContainsTags(item.tags, tags))
                        ) {
                            v.removeElementAt(curItem)
                        }
                    }
                    Types.KanjiToRu, Types.RuToKanji -> {
                        if (item.ru.isEmpty() || item.kanji.isEmpty() ||
                            (!isContainsTags(item.tags, tags))
                        ) {
                            v.removeElementAt(curItem)
                        }
                    }
                }
            }
            curItem--
        }
    }

    private fun isContainsTags(
        commonDatabaseItemTags: Vector<String>,
        userItemTags: Vector<String>
    ): Boolean {
        for (tag in userItemTags) {
            if (!commonDatabaseItemTags.contains(tag)) {
                return false
            }
        }
        return true
    }

    private fun removeNotNeedItems(v: Vector<CommonItem>, tags: Vector<String>) {
        var curItem = v.size - 1
        while (curItem >= 0) {
            if (!isContainsTags(v[curItem].tags, tags)) {
                v.removeElementAt(curItem)
            }
            curItem--
        }
    }

    private enum class ItemDbDefPos(val value: Int) {
        Id(0),
        Jp(1),
        Kanji(2),
        Tags(3),
        Ru(4);
    }

    private const val ITEM_DB_NUM = 5

    private enum class ItemUserProgressPos(val value: Int) {
        Id(0),
        JpToRuCounter(1),
        RuToJpCounter(2),
        KanjiToJpCounter(3),
        JpToKanjiCounter(4),
        KanjiToRuCounter(5),
        RuToKanjiCounter(6),
        Date(7)
    }

    private const val ITEM_USER_PROGRESS_NUM = 8

    private val userProgressHeaderItems = Vector(
        listOf(
            "id",
            "jp_to_ru_counter", "ru_to_jp_counter",
            "kanji_to_jp_counter", "jp_to_kanji_counter",
            "kanji_to_ru_counter", "ru_to_kanji_counter",
            "date"
        )
    )

    private val commonItems_: MutableMap<Int, CommonItem> = mutableMapOf()
    private val userItems_: MutableMap<Int, UserItem> = mutableMapOf()
}
