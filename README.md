# Jap - это приложение, позволяющая тренировать многие аспекты владения японским языком.
![alt text](fastlane/metadata/android/ru/images/icon.png)

# Jap - утилита для помощи в изучении японского языка
## Основные возможности
- Просмотр общего словаря с возможностью выбора из него требуемых для изучения слов и коротких фраз
- Просмотр личного словаря с прогрессом по каждому направлению изучения слов и коротких фраз
- Тренировки слов из личного словаря
    - Режим карт запоминания (6 вариантов тренировок)

<details>
    <summary>Демонстрационные экраны</summary>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/1.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/2.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/3.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/4.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/5.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/6.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/7.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/8.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/9.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/10.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/11.png" alt="drawing" width="200"/>
    <img src="fastlane/metadata/android/ru/images/phoneScreenshots/12.png" alt="drawing" width="200"/>
</details>